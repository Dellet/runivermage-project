import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/HomePage'
import AboutUs from "@/components/AboutUs";

import Catalog from '@/components/Catalog'
import Product from "@/components/Product";
import NewArrivals from "@/components/NewArrivals";
import Delivery from "@/components/Delivery";
import Cart from "@/components/Cart"

import Register from "@/components/Register";

import NotFound from "@/components/NotFound";

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      props: true,
      component: Home,
      meta: {
        breadcrumb: 'Главная',
        transitionName: 'slide'
      },
    },
    {
      path: '/catalog',
      name: 'catalog',
      component: Catalog,
      meta: {
        title: 'Каталог',
        breadcrumb() {
          // const { name } = this.$route;
          return {
            label: 'Каталог',
            parent: 'home'
          };
        }
      }
    },
    {
      path: '/catalog/:id',
      name: 'product',
      component: Product,
      meta: {
        breadcrumb() {
          return {
            label: 'Продукт',
            parent: 'catalog'
          };
        }
      }
    },
    {
      path: '/catalog/arrivals',
      name: 'arrivals',
      component: NewArrivals,
      meta: {
        title: 'Новинки',
        breadcrumb() {
          return {
            label: 'Новинки',
            parent: 'catalog'
          };
        }
      }
    },
    {
      path: '/about',
      name: 'about',
      component: AboutUs,
      meta: {
        title: 'Рунивермаг',
        breadcrumb() {
          return {
            label: 'О нас | Контакты',
            parent: 'home'
          };
        }
      }
    },
    {
      path: '/404',
      name: 'NotFound',
      component: NotFound
    },
    {
      path: '*', redirect: '/404'
    },
    {
      path: '/delivery',
      name: 'delivery',
      component: Delivery,
      meta: {
        title: 'Доставка и оплата',
        breadcrumb() {
          return {
            label: 'Доставка | Оплата',
            parent: 'home'
          };
        }
      }
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart,
      props: true,
      meta: {
        title: 'Оформление заказа',
        breadcrumb() {
          return {
            label: 'Оформление заказа',
            parent: 'home'
          };
        }
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        title: 'Регистрация',
        breadcrumb() {
          return {
            label: 'Регистрация',
            parent: 'home'
          }
        }
      }
    },
    {
      path: '/runodel',
      beforeEnter() {location.href = 'https://runodel-project.web.app/'}
    }
  ],

  // scrollBehavior() {
  //   document.getElementById('app').scrollIntoView({ behavior: 'smooth' });
  // },
  // scrollBehavior: (to, from, savedPosition) => {
  //   if (savedPosition) {
  //     return savedPosition;
  //   } else if (to.hash) {
  //     return {
  //       selector: to.hash
  //     };
  //   } else {
  //     return { top: 0 };
  //   }
  // },

  base: process.env.BASE_URL,
});

export default router;
