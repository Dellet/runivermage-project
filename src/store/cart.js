export default {
    state: {
        cart: []
    },
    //Setters - mutations and actions
    mutations: {
        SET_CART: (state, product) => {
            if(state.cart.length) {
                let isProductExists = false;
                state.cart.map(function(item) {
                    if (item.sku === product.sku) {
                        isProductExists = true;
                        item.quality++
                    }
                });
                if (!isProductExists) {
                    state.cart.push(product)
                }
            } else {
                state.cart.push(product)
            }
        },
        REMOVE_FROM_CART: (state, index) => {
            state.cart.splice(index, 1)
        }
    },
    actions: {
        ADD_TO_CART({commit}, product) {
            commit('SET_CART', product)
        },
        DELETE_FROM_CART({commit}, index) {
            commit('REMOVE_FROM_CART', index)
        }
    },
    //Getters - get data from state
    getters: {
        CART(state) {
            return state.cart
        }
    }
}
