import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import shop from "./shop"
import cart from "./cart"

export default new Vuex.Store({
    modules: {
        shop,
        cart
    }
})
