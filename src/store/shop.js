export default {
    state: {
        shopList: [
            {
                id: 1,
                title: 'Скандинавский амулет Древо жизни ',
                img: require('../assets/catalog-page/catalog-items/item-1.png'),
                sku: 'артикул: 65598100001',
                type: 'metal',
                rating: 4,
                availability: true,
                price: 1150,
                novelty: true,
            },
            {
                id: 2,
                title: 'Руна Вуньо (амулет)',
                img: require('../assets/catalog-page/catalog-items/item-2.png'),
                sku: 'артикул: 65975940001',
                type: 'metal',
                rating: 5,
                availability: true,
                price: 720,
                novelty: false,
            },
            {
                id: 3,
                title: 'Кельтский амулет Ванир Фрей Меч с рунами',
                img: require('../assets/catalog-page/catalog-items/item-3.png'),
                sku: 'артикул: 16470100001',
                type: 'wood',
                rating: 4,
                availability: false,
                price: 850,
                novelty: false,
            },
            {
                id: 4,
                title: 'Футарк',
                img: require('../assets/catalog-page/catalog-items/item-4.png'),
                sku: 'артикул: 36498100001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1450,
                novelty: false,
                sale: 30
            },
            {
                id: 5,
                title: 'Скандинавский амулет Кулон Удачи',
                img: require('../assets/catalog-page/catalog-items/item-5.png'),
                sku: 'артикул: 61651900001',
                type: 'metal',
                rating: 4,
                availability: true,
                price: 1270,
                novelty: true
            },
            {
                id: 6,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-6.png'),
                sku: 'артикул: 91820100001',
                type: 'metal',
                rating: 4,
                availability: false,
                price: 1040,
                novelty: false
            },
            {
                id: 7,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-7.png'),
                sku: 'артикул: 62981500001',
                type: 'metal',
                rating: 5,
                availability: true,
                price: 1020,
                novelty: false
            },
            {
                id: 8,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-8.png'),
                sku: 'артикул: 95067100001',
                type: 'wood',
                rating: 4,
                availability: true,
                price: 640,
                novelty: false,
                sale: 10
            },
            {
                id: 9,
                title: 'Скандинавский амулет Древо жизни',
                sku: 'артикул: 94684100001',
                img: require('../assets/catalog-page/catalog-items/item-9.png'),
                type: 'wood',
                rating: 5,
                availability: true,
                price: 880,
                novelty: true
            },
            {
                id: 10,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-10.png'),
                sku: 'артикул: 2649200001',
                type: 'skin',
                rating: 4,
                availability: true,
                price: 1100,
                novelty: false
            },
            {
                id: 11,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-11.png'),
                sku: 'артикул: 16824900001',
                type: 'metal',
                rating: 4,
                availability: true,
                price: 1620,
                novelty: false,
                sale: 10
            },
            {
                id: 12,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-12.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 5,
                availability: true,
                price: 1650,
                novelty: false
            },
            {
                id: 21617625,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-5.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 4,
                availability: true,
                price: 1450,
                novelty: false
            },
            {
                id: 12863615,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-7.png'),
                sku: 'артикул: 26491500001',
                type: 'skin',
                rating: 4,
                availability: true,
                price: 1550,
                novelty: false
            },
            {
                id: 72423719,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-1.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 5,
                availability: true,
                price: 1550,
                novelty: false
            },
            {
                id: 75964627,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-2.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 4,
                availability: true,
                price: 1600,
                novelty: false
            },
            {
                id: 73511445,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-6.png'),
                sku: 'артикул: 26491500001',
                type: 'metal',
                rating: 4,
                availability: true,
                price: 1980,
                novelty: false
            },
            {
                id: 63286542,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-8.png'),
                sku: 'артикул: 26491500001',
                type: 'metal',
                rating: 4,
                availability: true,
                price: 3300,
                novelty: true
            },
            {
                id: 73513445,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-10.png'),
                sku: 'артикул: 26491500001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1650,
                novelty: false
            },
            {
                id: 17532934,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-1.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 4,
                availability: true,
                price: 1615,
                novelty: false
            },
            {
                id: 51460774,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-2.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 4,
                availability: true,
                price: 655,
                novelty: false
            },
            {
                id: 69216902,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-8.png'),
                sku: 'артикул: 26491500001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1650,
                novelty: true
            },
            {
                id: 99499025,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-9.png'),
                sku: 'артикул: 26491500001',
                type: 'metal',
                rating: 4,
                availability: true,
                price: 1680,
                novelty: false
            },
            {
                id: 35179735,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-5.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 4,
                availability: true,
                price: 950,
                novelty: false
            },
            {
                id: 32530161,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-6.png'),
                sku: 'артикул: 26491500001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1600,
                novelty: false
            },
            {
                id: 11723979,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-3.png'),
                sku: 'артикул: 26491500001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1670,
                novelty: false
            },
            {
                id: 20819879,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-8.png'),
                sku: 'артикул: 26491500001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1090,
                novelty: false
            },
            {
                id: 36190789,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-3.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 4,
                availability: true,
                price: 1450,
                novelty: true
            },
            {
                id: 36110289,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-4.png'),
                sku: 'артикул: 26491500001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1950,
                novelty: false
            },
            {
                id: 24104911,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-7.png'),
                sku: 'артикул: 26491500001',
                type: 'metal',
                rating: 4,
                availability: true,
                price: 1100,
                novelty: false
            },
            {
                id: 60436788,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-10.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 4,
                availability: true,
                price: 1850,
                novelty: false
            },
            {
                id: 93751475,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-11.png'),
                sku: 'артикул: 26491500001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1690,
                novelty: false
            },
            {
                id: 91687492,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-9.png'),
                sku: 'артикул: 26491500001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1780,
                novelty: true
            },
            {
                id: 50432966,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-1.png'),
                sku: 'артикул: 26491500001',
                type: 'wood',
                rating: 4,
                availability: true,
                price: 1920,
                novelty: false
            },
            {
                id: 13169891,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-4.png'),
                sku: 'артикул: 26491500001',
                type: 'metal',
                rating: 4,
                availability: true,
                price: 1850,
                novelty: true
            },
            {
                id: 45598017,
                title: 'Скандинавский амулет Древо жизни',
                img: require('../assets/catalog-page/catalog-items/item-6.png'),
                sku: 'артикул: 26491500001',
                type: 'stone',
                rating: 4,
                availability: true,
                price: 1700,
                novelty: false
            }
        ]
    },
    //Setters - mutations and actions
    mutations: {
    },
    actions: {
    },
    //Getters - get data from state
    getters: {
        getShopList(state) {
            return state.shopList
        },
        getProduct: (state) => (id) => {
            console.log(typeof id)
            return state.shopList.find(product => product.id === parseFloat(id))
        }
    }
}
